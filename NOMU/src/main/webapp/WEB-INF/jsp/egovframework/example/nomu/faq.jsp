<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>

  <main class="container">
      <div class="faq_title"><strong>FAQ</strong></div>
      <div class="faq_category">
        <ul>
            <li><a href="#">노무챗봇</a></li>
            <li><a href="#">노무사정보</a></li>
            <li><a href="#">노무사전</a></li>
            <li><a href="#">판례</a></li>
            <li><a href="#">사례</a></li>
        </ul>
      </div>
      <div class="faq_table">
          <table>
              <thead>
                  <tr>
                      <th>분류</th>
                      <th>제목</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td><a href="#">노무챗봇</a></td>
                      <td>faq 제목이 표출됩니다</td>
                  </tr>
                  <tr>
                    <td><a href="#">노무챗봇</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">노무사전</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">노무사전</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">노무사정보</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">노무챗봇</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">판례</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">판례</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">사례</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">사례</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                <tr>
                    <td><a href="#">노무챗봇</a></td>
                    <td>faq 제목이 표출됩니다</td>
                </tr>
                  
              </tbody>
          </table>
      </div>
      <div class="faq_paging">
          <div class="ul_paging">
            <ul>
                <li><a href="#"><img src="images/nomu/leftArrow.png" alt="leftArrow"></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><img src="images/nomu/rightArrow.png" alt="rightArrow"></a></li>
            </ul>
          </div>
      </div>

  </main>
  <%@ include file="../includes/footer.jsp" %>


  <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.min.js"></script>
  <script type="text/javascript" src="js/paging.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paginationjs/2.1.4/pagination.min.js"></script>
    

</body>

</html>