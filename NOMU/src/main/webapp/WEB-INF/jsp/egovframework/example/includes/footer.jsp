<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<footer>
    <div class="container footer">
      <h1>NOMU</h1>
      <ul>
        <li><a href="#">개인정보처리방침</a> <span>|</span> <a href="#">이용약관</a></li>
        <li><a href="#">Adress : 서울 강남구 논현로 79길 59</a></li>
        <li><a href="#">E-mail : aaa @ nomu.co.kr</a></li>
        <li><a href="#">Copyright 2020 NOMU All rights reserved.</a></li>
      </ul>
    </div>
  </footer>