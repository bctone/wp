<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link rel="stylesheet" href="<c:url value='/'/>css/portal_css/default.css" type="text/css" >
<link href="<c:url value='${brdMstrVO.tmplatCours}' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value='/js/EgovBBSMng.js' />"></script>
<c:if test="${anonymous == 'true'}"><c:set var="prefix" value="/anonymous"/></c:if>
<%@ include file="../../includes/header.jsp" %>
<script type="text/javascript">
	function onloading() {
		if ("<c:out value='${msg}'/>" != "") {
			alert("<c:out value='${msg}'/>");
		}
	}
	
	function fn_egov_select_noticeList(pageNo) {
		document.frm.pageIndex.value = pageNo; 
		document.frm.action = "<c:url value='/cop/bbs${prefix}/selectBoardList.do'/>";
		document.frm.submit();	
	}
	
	function fn_egov_delete_notice() {
		if ("<c:out value='${anonymous}'/>" == "true" && document.frm.password.value == '') {
			alert('등록시 사용한 패스워드를 입력해 주세요.');
			document.frm.password.focus();
			return;
		}
		
		if (confirm('<spring:message code="common.delete.msg" />')) {
			document.frm.action = "<c:url value='/cop/bbs${prefix}/deleteBoardArticle.do'/>";
			document.frm.submit();
		}	
	}
	
	function fn_egov_moveUpdt_notice() {
		if ("<c:out value='${anonymous}'/>" == "true" && document.frm.password.value == '') {
			alert('등록시 사용한 패스워드를 입력해 주세요.');
			document.frm.password.focus();
			return;
		}

		document.frm.action = "<c:url value='/cop/bbs${prefix}/forUpdateBoardArticle.do'/>";
		document.frm.submit();			
	}
	
	function fn_egov_addReply() {
		document.frm.action = "<c:url value='/cop/bbs${prefix}/addReplyBoardArticle.do'/>";
		document.frm.submit();			
	}	
</script>
<c:if test="${useComment == 'true'}">
<c:import url="/cop/bbs/selectCommentList.do" charEncoding="utf-8">
	<c:param name="type" value="head" />
</c:import>
</c:if>
<c:if test="${useSatisfaction == 'true'}">
<c:import url="/cop/bbs/selectSatisfactionList.do" charEncoding="utf-8">
	<c:param name="type" value="head" />
</c:import>
</c:if>
<c:if test="${useScrap == 'true'}">
<script type="text/javascript">
	function fn_egov_addScrap() {
		document.frm.action = "<c:url value='/cop/bbs/addScrap.do'/>";
		document.frm.submit();			
	}
</script>
</c:if>
<title><c:out value='${result.bbsNm}'/> - 글조회</title>

<style type="text/css">
	h1 {font-size:12px;}
	caption {visibility:hidden; font-size:0; height:0; margin:0; padding:0; line-height:0;}
</style>

<!-- wrap start -->
<main class="container">
	<div class="flex">
<div id="wrap">
    <div id="bodywrap">
        <div id="middle_content">
            <div id="content_field"><!--contents start-->

			<form name="frm" method="post" action="<c:url value='/cop/bbs${prefix}/selectBoardList.do'/>">
			<input type="hidden" name="pageIndex" value="<c:out value='${searchVO.pageIndex}'/>">
			<input type="hidden" name="bbsId" value="<c:out value='${result.bbsId}'/>" >
			<input type="hidden" name="nttId" value="<c:out value='${result.nttId}'/>" >
			<input type="hidden" name="parnts" value="<c:out value='${result.parnts}'/>" >
			<input type="hidden" name="sortOrdr" value="<c:out value='${result.sortOrdr}'/>" >
			<input type="hidden" name="replyLc" value="<c:out value='${result.replyLc}'/>" >
			<input type="hidden" name="nttSj" value="<c:out value='${result.nttSj}'/>" >
			<input type="submit" id="invisible" class="invisible"/>

            <!-- sub title start -->
            <div><h2><c:out value='${result.bbsNm}'/> - 글조회</h2></div>
            <!-- sub title end -->
            
            <!--detail area start -->
            <div class="search_service">
                <div class="search_top_table">
                    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#ffffff" class="generalTable">
				      <tr> 
				        <td class="td_width">제목</td>
				        <td class="td_content" colspan="5"><c:out value="${result.nttSj}" />
				        </td>
				      </tr>
				      <tr> 
				        <td class="td_width">작성자</td>
				        <td class="td_width">
				            <c:out value="${result.frstRegisterNm}" />
				        </td>
				        <td class="td_width">작성시간</td>
				        <td class="td_width"><c:out value="${result.frstRegisterPnttm}" />
				        </td>
			            <td class="td_width">조회수</td>
				        <td class="td_content"><c:out value="${result.inqireCo}" />
				        </td>
				      </tr>    
				      <tr> 
				        <td class="td_width">글내용</td>
				        <td class="td_width" colspan="5">
				        <textarea id="nttCn" name="nttCn" class="textarea" cols="95" rows="100px" readonly="readonly" title="글내용" style="resize: none;"><c:out value="${result.nttCn}" escapeXml="false" /></textarea>
				        </td>
				      </tr>
				      <c:if test="${not empty result.atchFileId}">
				          <c:if test="${result.bbsAttrbCode == 'BBSA02'}">
				          <tr> 
				            <td class="td_width">첨부이미지</td>
				            <td class="td_content" colspan="5">
				                    <c:import url="/cmm/fms/selectImageFileInfs.do" charEncoding="utf-8">
				                        <c:param name="atchFileId" value="${result.atchFileId}" />
				                    </c:import>
				            </td>
				          </tr>
				          </c:if>
				          <tr> 
				            <td class="td_width">첨부파일 목록</td>
				            <td class="td_content" colspan="5">
				                <c:import url="/cmm/fms/selectFileInfs.do" charEncoding="utf-8">
				                    <c:param name="param_atchFileId" value="${result.atchFileId}" />
				                </c:import>
				            </td>
				          </tr>
				      </c:if>
				      <c:if test="${anonymous == 'true'}">
				      <tr> 
				        <td class="td_width"><label for="password"><spring:message code="cop.password" /></label></td>
				        <td class="td_content" colspan="5">
				            <input name="password" title="암호" type="password" size="20" value="" maxlength="20" >
				        </td>
				      </tr>
				      </c:if>   
				    </table>
                </div>
            </div>
            <!--detail area end -->
            
            <!-- 목록/저장버튼  시작-->
            <table border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
            <div class="buttons" align="center" style="margin-bottom:100px">
             <c:if test="${result.frstRegisterId == sessionUniqId}">     
                  <a href="#LINK" onclick="javascript:fn_egov_moveUpdt_notice(); return false;">수정</a> 
                  <a href="#LINK" onclick="javascript:fn_egov_delete_notice(); return false;">삭제</a> 
             </c:if>    
             <c:if test="${result.replyPosblAt == 'Y'}">     
                  <a href="#LINK" onclick="javascript:fn_egov_addReply(); return false;">답글작성</a> 
              </c:if>
              <a href="#LINK" onclick="javascript:fn_egov_select_noticeList('1'); return false;">목록</a> 
            </div>
            </td></tr></table>
            <!-- 목록/저장버튼  끝-->
            
            </form>

            </div><!-- contents end -->
        </div>
    </div>
    
    <!-- footer 시작 -->
    <%@ include file="../../includes/footer.jsp" %>
    <!-- //footer 끝 -->
</div>
</div>
<!-- //wrap end -->
</main>
</body>
</html>