<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>

	<main class="container clearfix">
		<div class="info_box">
			<div class="info_content1">
				<ul>
					<li class="info_tit">노무정보</li>
					<li class="info_sub_tit"><a href="/leadingcase.do">판례</a></li>
					<li class="info_sub_tit"><a href="/example.do">사례</a></li>
					<li class="info_sub_tit menuClick"><a href="/news.do">뉴스</a></li>
				</ul>
			</div>

			<div class="info_content2">
				<div class="input_area">
					<input type="text" placeholder="검색어를 입력해주세요">
					<button type="button">
						<i class="fas fa-search"></i>
					</button>
				</div>
				<div class="table">
					<table class="type01">
						<tr class="table_title">
							<th scope="row">번호</th>
							<td>뉴스</td>
						</tr>
						<c:forEach items="${news}" var="news" varStatus="status">
							<tr class="table_desc">
								<th scope="row">${status.count}</th>
								<td><strong>${news.title}</strong>
									<p>${news.content}</p> <a href="${news.link }"><button type="button">더보기</button></a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</main>

	<%@ include file="../includes/footer.jsp" %>


	<!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js"
		integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	
</body>

</html>
</body>
</html>