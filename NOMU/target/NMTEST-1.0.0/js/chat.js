function Message(arg) {
    this.text = arg.text;
    this.message_side = arg.message_side;
    this.chatTime = arg.chatTime;
    this.draw = function (_this) {
        return function () {
            let $message;
            $message = $($('.message_template').clone().html());
            $message.addClass(_this.message_side).find('.text').html(_this.text);
            $message.find('.chatting_time').html(_this.chatTime)
            $('.messages').append($message);
            return setTimeout(function () {
                return $message.addClass('appeared');
            }, 0);
        };
    }(this);
    return this;
}

//input value 받아오기
function getMessageText() {
    let $message_input;
    $message_input = $('.message_input');
    return $message_input.val();
}

//time
function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}




//채팅 띄우기
function sendMessage(text, message_side) {
    let date = formatAMPM(new Date());
    saveChatLog(text, 'save_log', message_side, date);
    let $messages, message;
    $('.message_input').val('');
    $messages = $('.messages');
    message = new Message({
        text: text,
        message_side: message_side,
        chatTime: date
    });
    message.draw();
    $messages.animate({ scrollTop: $messages.prop('scrollHeight') }, 300);
}



//처음 인사말
function greet() {
    setTimeout(function () {
        return sendMessage(
            `안녕하세요 저는 노무관련 상담을<br> 해주는 노무봇 입니다
            <br>노무관련 질문을 입력해주시거나 <br>하단의 카테고리를 선택해주세요<br>
            <div class=cate>
                <button class=cate_btn onclick=intentCategory('임금')>임금</button>
                <button class=cate_btn onclick=intentCategory('보험')>4대보험</button>
                <button class=cate_btn onclick=intentCategory('퇴직연금')>퇴직연금</button>
                <button class=cate_btn onclick=intentCategory('원천징수')>원천징수</button>
            </div>`
            , 'left');
    }, 500);
}

function onClickAsEnter(e) {
    if (e.keyCode === 13) {
        onSendButtonClicked()
    }
}

function requestRelationWord(middle, main){
    $('.relation p').empty();
    if(Array.isArray(main)){
        for(let i in main){
            $('.relation p').append(`<a href='#'><span>${main[i]}</span></a>`)
        }  
    }else{
        $('.relation p').append(`<a href='#'><span>${main}</span></a>`)
    }
    if(middle !== 'no'){
        $('.relation p').append(`<a href='#'><span>${middle}</span></a>`)
    }
}

function requestRecommend(word){
    $('.recommend_keyword p').empty();
    for(let i in word){
        $('.recommend_keyword p').append(`<a href='#'><span>${word[i]}</span></a>`)
    }  
}


function requestChat(messageText, url_pattern) {
    let msg = encodeURIComponent(messageText)
    $.ajax({
        url: "http://192.168.0.11:5000/" + url_pattern + "/" + msg,
        type: "GET",
        dataType: "json",
        error: function (request, status, error) {
            return sendMessage('죄송합니다. 무슨말인지 잘 모르겠어요.', 'left');
        }
    }).done(function(data){
        
        if (url_pattern === 'request_chat') {
            if(data.hasOwnProperty('leadingcase')){
                requestLeadingCase(data['leadingcase']);
            }
            if(data.hasOwnProperty('case')){
                requestCase(data['case']);
            }
            if(data['middleCategory'] != null){
                requestRelationWord(data['middleCategory'], data['mainCategory']);
            }else if(data['mainCategory'] != null){
                requestRelationWord('no', data['mainCategory']);
            }
            if(data['recommend'] != null){
                requestRecommend(data['recommend']);
            }
            for(let i in data['answer']){
                (function(x){
                  setTimeout(function(){
                    sendMessage(data['answer'][x], 'left');
                  }, 1000*x);
                })(i);
              }
        }else {
            return sendMessage('죄송합니다. 무슨말인지 잘 모르겠어요.', 'left');
        }
    })

}


function saveChatLog(messageText, url_pattern, side, time) {
    let msg = encodeURIComponent(messageText)
    $.ajax({
        url: "http://192.168.0.11:5000/" + url_pattern + "/" + msg + "/"+ side + "/" + time,
        type: "GET",
        dataType: "json",
        
    }).done(function(data){
        
      console.log(data);
    });
}

function requestLeadingCase(leadingcase) {
    leadingcaseContent = "";
    if(leadingcase == null){
        leadingcaseContent = "<p>아직 판례가 없습니다.</p>";
    }else{
        leadingcaseContent = leadingcase
    }
    $('.judicial p').empty()
    $('.judicial').append(`<p>${leadingcaseContent}</p>`)
}

function requestCase(exampleCase) {
    caseContent = "";
    if(exampleCase == null){
        caseContent = "<p>아직 사례가 없습니다.</p>";
    }else{
        caseContent = exampleCase
    }
    $('.law p').empty()
    $('.law').append(`<p>${exampleCase}</p>`)
}

function onSendButtonClicked() {
    let messageText = getMessageText();
    sendMessage(messageText, 'right');
    if (messageText.includes('/')) {
        setTimeout(function () {
            return sendMessage("죄송합니다. 무슨말인지 잘 모르겠어요.", 'left');
        }, 1000);
    }else if (messageText.includes('안녕')) {
        setTimeout(function () {
            return sendMessage("안녕하세요. 저는 노무상담 채팅봇 입니다.", 'left');
        }, 1000);
    } else if (messageText.includes('고마워')) {
        setTimeout(function () {
            return sendMessage("천만에요. 더 물어보실 건 없나요?", 'left');
        }, 1000);
    } else if (messageText.includes('없어')) {
        setTimeout(function () {
            return sendMessage("그렇군요. 알겠습니다!", 'left');
        }, 1000);
    }else if(messageText === ('처음')){
		greet();
	}else if(messageText == ('지우기')){
		$('.messages').empty();
		greet();
	}else{
        return requestChat(messageText, 'request_chat');
    }
}

greet();

let btnQuestion = {
    '임금1' : '고정연장수당 및 식대도 통상임금으로 보아야 하나요?',
    '임금2' :'만근수당은 통상임금에 포함되나요?',
    '원천징수1' :'특정월 중도 퇴사자의 경우 급여 계산 방법은?',
    '원천징수2' :'월급제 직원의 시급 계산은 어떻게 하는지요?',
    '보험1' :'보수월액 변경신고, 반드시 해야 하나요?',
    '보험2' :'월급제 직원의 시급 계산은 어떻게 하는지요?',
    '보험2' :'중도입사자에 대한 건강보험료 정산이 전부 환급이 나온 이유가 있나요?',
    '퇴직연금1' :'1년 미만 근로자가 관계사 전출 시 퇴직금은 어떻게 처리해야 하나요?',
    '퇴직연금2' :'근로시간 단축으로 퇴직금이 감소되면 중간정산이 가능한가요?',
}

function intentCategory(intent){
    if(intent === '임금'){
        sendMessage(`
            임금 관련 질문입니다.<br>
            1. ${btnQuestion['임금1']}
            <button class=answer onclick=answerBtnQuestion('임금1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['임금2']}
            <button class=answer onclick=answerBtnQuestion('임금2')>답변</button>
            `, 'left');
    }else if(intent === '보험'){
        sendMessage(`
            4대보험 관련 질문입니다.<br>
            1. ${btnQuestion['보험1']}
            <button class=answer onclick=answerBtnQuestion('보험1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['보험2']}
            <button class=answer onclick=answerBtnQuestion('보험2')>답변</button>
            `, 'left');
    }else if(intent === '원천징수'){
    
        sendMessage(`
            원천징수 관련 질문입니다.<br>
            1. ${btnQuestion['원천징수1']}
            <button class=answer onclick=answerBtnQuestion('원천징수1')>답변</button>
            `, 'left');
        sendMessage(`
            2. ${btnQuestion['원천징수2']}
            <button class=answer onclick=answerBtnQuestion('원천징수2')>답변</button>
            `, 'left');
    }else if(intent === '퇴직연금'){
        sendMessage(`
        퇴직연금 관련 질문입니다.<br>
        1. ${btnQuestion['원천징수1']}
        <button class=answer onclick=answerBtnQuestion('퇴직연금1')>답변</button>
        `, 'left');
        sendMessage(`
        2. ${btnQuestion['원천징수2']}
        <button class=answer onclick=answerBtnQuestion('퇴직연금2')>답변</button>
        `, 'left');
    }
}

$('.btn_prev').click(function(){
    greet();
});
$('.btn_clear').click(function(){
    $('.messages li').remove();
});

function answerBtnQuestion(question){
    sendMessage(btnQuestion[question], 'right');
    setTimeout(function () {
        requestChat(btnQuestion[question], 'request_chat');
    }, 1000);
}