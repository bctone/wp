<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>    
<!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/chat.js"></script> -->
<%@ page import ="egovframework.com.cmm.LoginVO" %>
<%@ include file="../includes/header.jsp" %>
	<main class="container">
    <div class="flex">
      <div class="content1">
        <div class="add_service">
          <div class="add_box">
            <h1>부가서비스</h1>
            <div class="labor_info">
              <h2>노무사 정보</h2>
                <div class="info">
                  <ul>
                    <li><a href="#" class="name">홍길동</a>/<span class="type">임금체불전문</span></li>
                    <li><a href="#" class="desc">임금/퇴직금</a><span class="time">(10분)</span></li>
                    <li><a href="#" class="price">10,000원</a></li>
                  </ul>
                </div>
              </div>
            <div class="job_info">
              <h2>일자리 정보</h2>

            </div>
            <div class="edu_info">
              <h2>교육 정보</h2>
            </div>
          </div>
        </div>
        <div class="note">
          <div class="note_box">
            <div class="notice">
            
              <div class="note_head">
                <h2>게시판</h2>
                <a href="<c:url value='/cop/bbs/selectBoardList.do?bbsId=BBSMSTR_BBBBBBBBBBBB'/>"><button type="button">더보기 +</button></a>
              </div>
              <div class="borad">
              <ol>
				    <c:set var="index" value="1"/>
				    <c:forEach var="result" items="${bbsList}" varStatus="status">
				    <li><img src="<c:url value='/'/>images/header/num0${index}.gif" alt="" />
				        <a href="<c:url value='/cop/bbs/selectBoardArticle.do?bbsId=BBSMSTR_BBBBBBBBBBBB&amp;nttId=${result.nttId}&amp;bbsTyCode=BBST01&amp;bbsAttrbCode=BBSA02&amp;authFlag=Y&amp;pageIndex=1'/>" onclick="fn_egov_regist_notice('<c:url value='${result.nttId}'/>'); return false;">
				        <c:out value="${fn:substring(fn:escapeXml(result.nttSj), 0, 18)}" />
				        </a>
				    </li>
				    <c:set var="index" value="${index+1}"/>
                    </c:forEach>
                    <c:if test="${fn:length(bbsList) == 0}" >
                        <li>최신 게시물이 없습니다.</li>
                    </c:if>
				</ol>
              </div>
            </div>
            <div id="faq">
              <div class="note_head">
               <form name="FaqListForm" action="<c:url value='/uss/olh/faq/FaqListInqire.do'/>" method="post">
	                <input name="faqId" type="hidden" value="">
					<input name="pageIndex" type="hidden" value="<c:out value='${searchVO.pageIndex}'/>"/>
                <h2>FAQ</h2>
               </form>
                <a href="<c:url value='/uss/olh/faq/FaqListInqire.do'/>"><button type="button">더보기 +</button></a>
              </div>
				<div class="search_result_div">
					<ul>
						<c:forEach items="${resultList}" var="resultInfo" varStatus="status">
				            <li class="q"><a href="<c:url value='/uss/olh/faq/FaqListDetailInqire.do?faqId=${resultInfo.faqId}&amp;pageIndex=${searchVO.pageIndex}'/>" onclick="fn_egov_inquire_faqlistdetail('<c:url value='${resultInfo.faqId}'/>'); return false;">
				                <c:out value="${resultInfo.qestnSj}"/>
				            </a></li>
                </c:forEach>
					</ul>
				</div>
            </div>
          </div>
        </div>
      </div>

      <div class="content2">
        <!--챗봇-->
        <div class="chat_window">
          <div class="top_menu">
            <div class="chat_btn">
              <div class="left">
                <button type="button"><img src="../../images/nomu/chat_question.png" alt="clear"></button>
              </div>
              <div class="right">
                <button type="button" class="btn_prev"><img src="../../images/nomu/chat_prev.png" alt="clear"></button>
                <button type="button" class="btn_clear"><img src="../../images/nomu/chat_clear.png" alt="clear"></button>
              </div>
            </div>
          </div>
          <ul class="messages"></ul>
          <div class="category">
          </div>
          <div class="bottom_wrapper">
            <div class="input_box">
              <input class="message_input" onkeyup="return onClickAsEnter(event)" placeholder="질문을 입력해주세요." />
              <div class="send_message" id="send_message" onclick="onSendButtonClicked()">
  
                <div class="icon"></div>
                <div class="text"><img src="../../images/nomu/submit.png" alt="submit"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="message_template">
          <li class="message">
            <div class="chat_text_time">
              <div class="avatar"></div>
              <div class="text_wrapper">
                <div class="text"></div>
              </div>
              <div class="chatting_time"></div>
            </div>
          </li>
        </div>
        <!--챗봇-->
      </div>
      
      <div class="content3">
        <div class="con_box">
          <div class="judicial">
            <div class="news_head">
              <h2>판례</h2>
              <a href='<c:url value="/leadingcase.do"/>'><button type="button">더보기 +</button></a>
            </div>
            <p>
              우위위이이잉. 오전 7시 정각이 되자 거대한
              소음과 함께 육중한 기계가 구르기 시작했다.
              몇 분쯤 지났을까. 공회전하던 50m 길이의
              철제 레일 위로 크고 작은 택배 상자가 하나둘
              실려나왔다. 레일 앞에 선 심복선씨(42)의
              눈과 손이 분주해졌다 공회전하던 50m 길이의
              철제 레일 위로 크고 작은 택배 상자가 하나둘
              실려나왔다. 레일 앞에 선 심복선씨(42)의
              눈과 손이 분주해졌다
            </p>
          </div>
          <div class="case">
            <div class="news_head">
              <h2>사례</h2>
              <a href='<c:url value="/example.do"/>'><button type="button">더보기 +</button></a>
            </div>
            <p>
              최근 유난히 업무량이 많이 늘어난 곳이 있다면
              바로 비대면서비스를 해야 하는 곳들과 배달이나
              택배업을 하는 직종이라고 합니다. 택배 폭증에
              기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
              택배 폭증에 기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
            </p>
          </div>
          <!-- 
          <div class="news">
          	<div class="news_head">
          		<c:forEach items="${news}" var="news" varStatus="status" begin="0" end="0">
					<h2>뉴스</h2>
					<a href="${news.link}"><button type="button">더보기 +</button></a>
			</div>
					<strong>${news.title}</strong>
					<p>${news.content}</p>
		  		</c:forEach>
		  </div>
           -->
          <div class="news">
            <div class="news_head">
              <h2>뉴스</h2>
              <a href='<c:url value="/news.do"/>'><button type="button">더보기 +</button></a>
            </div>
            <strong>택배기사 뇌출혈산재 승인 사례</strong>
            <p>
              최근 유난히 업무량이 많이 늘어난 곳이 있다면
              바로 비대면 서비스를 해야 하는 곳들과 배달
              이나 택배업을 하는 직종이라고 합니다.
              택배 폭증에 기사분들의 과로 정도가 크다고하며
              신규 기사에게는 이에 따라 6-70%만 업무를
              배정하도록 하고 택배차량과 기사를 신속하 처리한다
              택배 폭증에 기사분들의 과로 정도가 크다고하며 신규 기사에
              게는 이에 따라 6-70%만 업무를 배정하도록
              하고 택배차량과 기사를 신속하게 처리 한다
            </p>
          </div>
        </div>
      </div>
      <div class="content4">
        <div class="login">
          <div class="login_box">
    		<%
               LoginVO loginVO = (LoginVO)session.getAttribute("LoginVO");
               if(loginVO != null){
            %>
      		<c:set var="loginName" value="<%= loginVO.getName()%>"/>
    		<ul>
            	<li><a href="#LINK" onclick="alert('개인정보 확인 등의 링크 제공'); return false;"><c:out value="${loginName}"/>님</a> 로그인하셨습니다.</li>
    		</ul>
    		<%  } %>
	     <%
           if(loginVO == null){
        %>
        <a href="<c:url value='/uat/uia/egovLoginUsr.do'/>"><button type="submit">로그인</button></a>
        <a href="<c:url value='/uss/umt/cmm/EgovMberSbscrbView.do'/>"><button type="submit">회원가입</button></a>
        <% }else{ %>
        <a href="<c:url value='/uat/uia/actionLogout.do'/>"><button type="button">로그아웃</button></a>
        <% } %>
        </div>
        </div>
        <div class="cloud">
        	<img src="../../images/wordcloud/wcloud.jpg">
        </div>
        <div class="mycontent">
          <div class="my_box">
            <div class="dictionary">
              <div class="dict_box">
                <h2>노무사전</h2>
              </div>
              <p>
                <a href="#">#<span>실업급여</span></a>
              </p>
            </div>
            <div class="recommend_keyword">
                <div class="dict_box">
                  <h2>추천키워드</h2>
                </div>
                <p>
                  <a href="#">#<span>코로나19</span></a>
                </p>
              </div>
            <div class="relation">
              <div class="dict_box">
                <h2>연관키워드</h2>
              </div>
              <p>
                <a href="#">#<span>고용노동부</span></a>
                <a href="#">#<span>산재보험</span></a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

<%@ include file="../includes/footer.jsp" %>
  </main>


<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  
<!-- 로그인 관련 javascript -->
  <script type="text/javascript">
function actionLogin() {
    if (document.loginForm.id.value =="") {
        alert("아이디를 입력하세요");
    } else if (document.loginForm.password.value =="") {
        alert("비밀번호를 입력하세요");
    } else {
        document.loginForm.action="<c:url value='/uat/uia/actionSecurityLogin.do'/>";
        //document.loginForm.j_username.value = document.loginForm.userSe.value + document.loginForm.username.value;
        //document.loginForm.action="<c:url value='/j_spring_security_check'/>";
        document.loginForm.submit();
    }
}


function goRegiUsr() {
    var userSe = document.loginForm.userSe.value;
    // 일반회원
    if (userSe == "GNR") {
        document.loginForm.action="<c:url value='/uss/umt/cmm/EgovMberSbscrbView.do'/>";
        document.loginForm.submit();
    }else{
        alert("일반회원 가입만 허용됩니다.");
    }
}

function setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name) {
    var search = Name + "="
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search)
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset)
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    return "";
}

function saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
    if (form.checkId.checked)
        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
    else
        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
    setCookie("saveid", form.id.value, expdate);
}

function getid(form) {
    form.checkId.checked = ((form.id.value = getCookie("saveid")) != "");
}

function fnInit() {
    var message = document.loginForm.message.value;
    if (message != "") {
        alert(message);
    }
    
    getid(document.loginForm);
}
function goMenuPage(menuNo){
    document.getElementById("menuNo").value=menuNo;
    document.getElementById("link").value="forward:"+getLastLink(menuNo);
    document.menuListForm.action = "<c:url value='/EgovPageLink.do'/>";
    document.menuListForm.submit();
}

$('.btn_prev').click(function(){
    greet();
});
$('.btn_clear').click(function(){
    $('.messages li').remove();
});

function fn_egov_inquire_faqlistdetail(faqId) {		

	// Faqid
	document.FaqListForm.faqId.value = faqId;
	document.FaqListForm.pageIndex.value = 1;
  	document.FaqListForm.action = "<c:url value='/uss/olh/faq/FaqListDetailInqire.do'/>";  	
  	document.FaqListForm.submit();	
	   	   		
}

function fn_egov_regist_notice(){
	//document.board.onsubmit();

	if (!validateBoard(document.board)){
		return;
	}
	
	if (confirm('<spring:message code="common.update.msg" />')) {
		document.board.action = "<c:url value='/cop/bbs${prefix}/updateBoardArticle.do'/>";
		document.board.submit();					
	}
}

</script>
  
 </body>
</html>
